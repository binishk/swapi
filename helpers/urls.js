var createError = require("http-errors");

function replace(url) {
  return url ? url.replace("swapi.dev", `localhost:${5000}`) : null;
}

function process(urls) {
  return Array.isArray(urls) ? urls.map((url) => replace(url)) : replace(urls);
}

/* Each element has unique relationship with the others
find each relationship and parse them accordingly */

function parseRelationships(results, type) {
  // Check if results is an array, convert into array otherwise
  results = Array.isArray(results) ? results : [results];
  switch (type) {
    case "people":
      return results.map((result) => {
        return {
          ...result,
          url: process(result.url),
          homeworld: process(result.homeworld),
          films: process(result.films),
          species: process(result.species),
          vehicles: process(result.vehicles),
          starships: process(result.starships),
        };
      });
    case "films":
      return results.map((result) => {
        return {
          ...result,
          url: process(result.url),
          planets: process(result.planets),
          species: process(result.species),
          characters: process(result.characters),
          vehicles: process(result.vehicles),
          starships: process(result.starships),
        };
      });
    case "vehicles":
    case "starships":
      return results.map((result) => {
        return {
          ...result,
          url: process(result.url),
          films: process(result.fimls),
          pilots: process(result.pilots),
        };
      });
    case "species":
      return results.map((result) => {
        return {
          ...result,
          url: process(result.url),
          films: process(result.fimls),
          people: process(result.people),
          homeworld: process(result.homeworld),
        };
      });
    case "planets":
      return results.map((result) => {
        return {
          ...result,
          url: process(result.url),
          films: process(result.fimls),
          residents: process(result.residents),
        };
      });
    default:
      throw createError(400);
  }
}

function parseAll(obj, type) {
  return Object.assign(
    { data: { type: type, attributes: parseRelationships(obj.results, type) } },
    {
      links: {
        count: obj.count,
        next: replace(obj.next),
        previous: replace(obj.previous),
      },
    }
  );
}

function parseOne(obj, type, id) {
  return Object.assign({
    data: { type: type, id: id, attributes: parseRelationships(obj, type)[0] },
  });
}

exports.parseAll = parseAll;
exports.parseOne = parseOne;
