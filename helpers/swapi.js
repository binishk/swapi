var axios = require("axios");

async function getAll(type, search, page) {
  var searchQuery = search ? `?search=${search}` : ``;
  var pageQuery = page ? `${search ? "&" : "?"}page=${page}` : ``;
  try {
    var result = await axios.get(
      `https://swapi.dev/api/${type}/${searchQuery}${pageQuery}`
    );
    return result.data;
  } catch (err) {
    throw err;
  }
}

async function getContent(type, id) {
  try {
    var result = await axios.get(`https://swapi.dev/api/${type}/${id}/`);
    return result.data;
  } catch (err) {
    throw err;
  }
}

exports.getContent = getContent;
exports.getAll = getAll;
