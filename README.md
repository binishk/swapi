## A wrapper API around the SWAPI api following specifications from JSON:API.

### Instructions to Run
Clone or download the project and run

```
npm install
```
```
npm start
```
By default runs on 5000 port.
To run on a specific port:
```
PORT={PORT_NUMBER} npm start
```

### Endpoints:

http://localhost:5000/  
http://localhost:5000/{item}/  
http://localhost:5000/{item}/:id/  
where _item_ can refer to any one of the following:
___people, planets, species, vehicles, starships, films___

To search for specific resource:  
http://localhost:5000/?search=luke  

Pagination:  
http://localhost:5000/?page=2  

### JSON API:
Requires use of the JSON:API media type (application/vnd.api+json) for exchanging data.  
Succesful response contains data field.
Errored response contains errors field.
Both are mutually exclusive.  

### Errors:

| Error        | Code           | Reason  |
| ------------- |:-------------:| -----:|
| Internal Server | 500 | General errors in the server |
| Not Found       | 404    | incorrect url pointing to a non-existing resource |
| Not Acceptable  | 406  |  Accept header media type modified with parameters|
| Unsupported Media Type | 415 | Content-type header has medie parameters


## References

https://swapi.dev/  
https://jsonapi.org/

