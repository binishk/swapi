var createError = require("http-errors");

// standard type for JSON:API format
const DATA_TYPE = "application/vnd.api+json";

function validateHeader(req, res, next) {
  try {
    /* return Not Acceptable error (406) if incorrect accept header
    or media types specified */
    if (req.accepts("application/vnd.api+json") !== DATA_TYPE) {
      throw createError(406);
    }

    /* get methods work without specifying content type
     otherwise return Unsupported Media Type error (415)
     if incorrect type in content-type header */
    if (req.method !== "GET") {
      if (
        !req.headers["content-type"] ||
        req.headers["content-type"].indexOf(DATA_TYPE) === -1
      ) {
        throw createError(415);
      }
    }
    next();
  } catch (err) {
    next(err);
  }
}

function setJSONHeader(req, res, next) {
  res.setHeader("Content-Type", "application/vnd.api+json; charset=utf-8");
  next();
}

// handle 404 routes
function handleNotFoundError(req, res, next) {
  next(createError(404, "Resource Not Found"));
}

// handle rest of the errors
function handleErrors(err, req, res, next) {
  err = req.app.get("env") === "development" ? err : {};
  res.status(err.status || 500).json({ title: err.message });
}

exports.validateHeader = validateHeader;
exports.setJSONHeader = setJSONHeader;
exports.handleErrors = handleErrors;
exports.handleNotFoundError = handleNotFoundError;
