var express = require("express");
var cors = require("cors");
var setRoutes = require("./routes");
var { validateHeader, setJSONHeader, handleNotFoundError, handleErrors} = require("./middlewares");

var app = express();
app.use(cors());
app.use(validateHeader);
app.use(setJSONHeader);

setRoutes(app);

// catch 404 and forward to error handler
app.use(handleNotFoundError);

// error handler
app.use(handleErrors);

module.exports = app;
