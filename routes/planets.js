var router = require("express").Router();
var swapi = require("../helpers/swapi");
var urls = require("../helpers/urls");

/* GET all planets. */
router.get("/", async function (req, res, next) {
  try {
    var result = await swapi.getAll("planets");
    res.status(200).json(urls.parseAll(result, "planets"));
  } catch (err) {
    next(err);
  }
});

/* Get planets by id */
router.get("/:id", async function (req, res, next) {
  try {
    var result = await swapi.getContent("planets", req.params.id);
    res.status(200).json(urls.parseOne(result, "planets", req.params.id));
  } catch (err) {
    next(err);
  }
});

module.exports = router;
