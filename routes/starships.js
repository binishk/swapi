var router = require("express").Router();
var swapi = require("../helpers/swapi");
var urls = require("../helpers/urls");

/* GET all starships. */
router.get("/", async function (req, res, next) {
  try {
    var result = await swapi.getAll("starships");
    res.status(200).json(urls.parseAll(result, "starships"));
  } catch (err) {
    next(err);
  }
});

/* Get starships by id */
router.get("/:id", async function (req, res, next) {
  try {
    var result = await swapi.getContent("starships", req.params.id);
    res.status(200).json(urls.parseOne(result, "starships", req.params.id));
  } catch (err) {
    next(err);
  }
});

module.exports = router;
