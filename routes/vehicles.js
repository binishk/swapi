var router = require("express").Router();
var swapi = require("../helpers/swapi");
var urls = require("../helpers/urls");

/* GET all vehicles. */
router.get("/", async function (req, res, next) {
  try {
    var result = await swapi.getAll("vehicles");
    res.status(200).json(urls.parseAll(result, "vehicles"));
  } catch (err) {
    next(err);
  }
});

/* Get vehicles by id */
router.get("/:id", async function (req, res, next) {
  try {
    var result = await swapi.getContent("vehicles", req.params.id);
    res.status(200).json(urls.parseOne(result, "vehicles", req.params.id));
  } catch (err) {
    next(err);
  }
});

module.exports = router;
