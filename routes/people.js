var router = require("express").Router();
var swapi = require("../helpers/swapi");
var urls = require("../helpers/urls");

/* GET all people. */
router.get("/", async function (req, res, next) {
  try {
    var result = await swapi.getAll("people", req.query.filter, req.query.page);
    res.status(200).json(urls.parseAll(result, "people"));
  } catch (err) {
    next(err);
  }
});

/* Get people by id */
router.get("/:id", async function (req, res, next) {
  try {
    var result = await swapi.getContent("people", req.params.id);
    res.status(200).json(urls.parseOne(result, "people", req.params.id));
  } catch (err) {
    next(err);
  }
});

module.exports = router;
