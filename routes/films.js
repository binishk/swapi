var router = require("express").Router();
var swapi = require("../helpers/swapi");
var urls = require("../helpers/urls");

/* GET all films. */
router.get("/", async function (req, res, next) {
  try {
    var result = await swapi.getAll("films");
    res.status(200).json(urls.parseAll(result, "films"));
  } catch (err) {
    next(err);
  }
});

/* Get films by id */
router.get("/:id", async function (req, res, next) {
  try {
    var result = await swapi.getContent("films", req.params.id);
    res.status(200).json(urls.parseOne(result, "films", req.params.id));
  } catch (err) {
    next(err);
  }
});

module.exports = router;
