const app = require("../app");

var router = require("express").Router()

router.get("/", function(req, res){
  res.json({
    "films": `http://localhost:${process.env.PORT||5000}/api/films/`,
    "people": `http://localhost:${process.env.PORT||5000}/api/people/`,
    "planets": `http://localhost:${process.env.PORT||5000}/api/planets/`,
    "species": `http://localhost:${process.env.PORT||5000}/api/species/`,
    "starships": `http://localhost:${process.env.PORT||5000}/api/starships/`,
    "vehicles": `http://localhost:${process.env.PORT||5000}/api/vehicles/`,
  })
})

module.exports = function (app) {
  app.use("/", router);
  app.use("/api/people", require("./people"));
  app.use("/api/films", require("./films"));
  app.use("/api/planets", require("./planets"));
  app.use("/api/species", require("./species"));
  app.use("/api/starships", require("./starships"));
  app.use("/api/vehicles", require("./vehicles"));
};
